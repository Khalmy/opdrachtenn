<?php

class Houses {

    public string $floor;
    public string $rooms;
    public string $width;
    public string $height;
    public string $depth;
    public string $volume;

    public function __construct (string $floor, string $rooms, string $width, string $height, string $depth) {
        $this->floor = $floor;
        $this->rooms = $rooms;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;

    }

    public function setVolume(string $width, string $height, string $depth) {

        return $this->volume = $width * $height * $depth;
    }

    public function getHouses() {
        $this->setVolume($this->width, $this->height, $this->depth);
        return "Dit huis heeft " . $this->floor . " verdiepingen, " . $this->rooms . " kamers en heeft een volume van " . $this->volume . " m3 " . "<br>" .
            "De prijs van dit huis is : " . $this->getPrice() . "<br>";

    }

    public function getPrice() {
        return $this->volume * 6200;
    }

}

$FirstHouses = new Houses(3, 8, 6, 2.5, 4);
$FirstHouses->setVolume(6, 2.5, 4);

$SecondHouses = new Houses(4, 10, 7, 4, 10);
$SecondHouses->setVolume(7,4,10);

$ThirdHouses = new Houses(2, 4,3,6,4);
$ThirdHouses->setVolume(3,6,4);

echo $FirstHouses->getHouses();
echo $SecondHouses->getHouses();
echo $ThirdHouses->getHouses();
